#### Packages ####

library(shiny)
library(tidyverse)
library(lubridate)

#### Import default datasets ####

# Public holliday
holliday <- read.csv(file = "data/Jours_feries.csv", stringsAsFactors = F, encoding = "UTF-8")
holliday$date <- ymd(holliday$date)

# Import default protocol
protocol_def <- readxl::read_excel(path = "data/protocole_gardes.xlsx")

# Import test list of resident
resident_def <- readxl::read_excel(path = "data/test_internes.xlsx")
colnames(resident_def) <- tolower(colnames(resident_def))


#' Balancing resident on call duty
#'
#' @param resident  data.farame of resident, with 3 colunm in french: nom, semestre and service
#' @param protocol  data.frame of the on-call protocol in long format with
#' @param weight integer of length one indicating the totale amount of work for all 
#' the on call planning by all the residents
#'
#' @return resident dataframe with a two new column 'coef' and 'todo' 
#' indicating respectively the proportion and the amount of work that each resident need to do.
#' 
#' @export 
#'
#' @examples
balancing <- function( resident, protocol, weight)
{
  # Function : balancing
  # ----------------- #
  # From the list of resident and the on-call protocol compute the adequate number of day/weight to do by resident.
  #
  # Args : 
  #   resident :
  #   protocol : 
  #   weights : units assigned to sudent
  # 
  # Return :
  #   
  
  todo <- vector(mode = "integer", length = length(resident$nom))
  coef <- vector(mode = "integer", length = length(resident$nom))
  error <- vector(mode = "character", length = length(resident$nom))
  # Get on-call coefficient for each resident
  for (i in seq_along(resident$nom)){
    if(resident$service[i] %in% unique(protocol$service)){
      if(any(protocol$semestre[protocol$service == resident$service[i] ] == resident$semestre[i])){
        coef[i] = protocol$nb_gardes[protocol$semestre == resident$semestre[i] &
                                       protocol$service == resident$service[i] ]
      } else { 
        coef[i] = NA
        error[i] <- "Semestre trop élevé pour cette unité / DES (ex : semestre 9 en medecine générale)"
      }
    } else {
      coef[i] <- NA
      error[i] <- "Service qui n'existe pas dans le protocole"
    }
  }
  
  # Then sum all the coefficients
  todo <- coef * weight/sum(coef, na.rm = T)
  
  return(cbind(resident, coef, todo, error))
}


##### Assignement function ####


#' Assign on call duty to medical student
#'
#' @param res_names vector, residents ID like names, surnames or anything else 
#' @param res_todo vector, number of on call to do. cf balancing function
#' @param planning data.frame or tbl, planning to fill from balancing function
#' @param id_we logical indication resident who are on duty on week end, they ar prioritise to have thursday on call day.
#' From balacing function : `!(resident$coef == 0.5  & resident$week_end == F)`
#' @param n.list number of list to create
#'
#' @return list, done is the amount of work done by resident, and planning the completed planning with names
#' @export
#'
#' @examples
#' 
assign <- function(res_names, res_todo, planning, id_we, n.list, rest = 14){
  
  # Initialise data for loops
  oncal_list <- vector("list", n.list) %>% 
    map(function(x)  x <- vector(mode = "character", length = length(planning$date)) )
  names(oncal_list) <- paste0("list", seq_len(n.list))
  
  day <-  seq_along(planning$date)
  
  available <- vector(mode = "integer", length = length(res_names))
  done <- rep(0,length(res_names))
  
  # Verify data
  id <- seq_along(res_names)
  
  if(missing(id_we)){  
    id_we <- id
  } else { 
    id_we <- id[id_we] 
  }
  
  # Week-end completion first
  for( d in day[planning$weight >= 1.5] ){
    
    # Get resident ID of available resident on a day :
    id.res <- id[id_we]
    id.quota <- id[ res_todo > done]
    id.comb <- id.res[id.res %in% id.quota]
    
    id.rest <-  map(oncal_list, 
                     function(x){
                       rest_day <- (day > (d - rest) & day < (d + rest))
                       not_available <- x[ rest_day ]
                       which(res_names %in% not_available)
                     } )  %>% 
      unlist() %>% 
      as.numeric() %>%
      unique()
    
    id.final <- id.comb[! id.comb %in% id.rest]

    if(length(id.final) < n.list){
      day_res <- c(id.final, rep(NA, n.list))[seq_len(n.list)]
      warning("Pas assez d'interne pour assurer l'ensemble de gardes selon le protocole.")
    } else {
      nb_res_min <- sum(done[id.final] == min(done[id.final]))
      
      if( nb_res_min < n.list ) {
        day_res <- c( id.final[ done[id.final] == min(done[id.final]) ], 
                      sample( id.final[ done[id.final] > min(done[id.final]) ], 
                              n.list - nb_res_min) )
      } else {
        
        day_res <- sample(id.final[ done[id.final] == min(done[id.final]) ], 
                          size = n.list )
      }
    }

    oncal_list <- map2(oncal_list, res_names[day_res], function(x,y){
      x[d] <- y 
      x } )
    done[day_res] <- done[day_res] + planning$weight[d]
  }
  
  #Week completion
  for( d in day[planning$weight[day] < 1.5] ){
    
    # Get resident ID of available resident on a day :
    id.res <- id
    id.quota <- id[ res_todo > done]
    id.comb <- id.res[id.res %in% id.quota]
    
    id.rest <-  map(oncal_list, 
                    function(x){
                      rest_day <- (day > (d - rest) & day < (d + rest)) # ajouter variable nombre de jour de repos
                      not_available <- x[ rest_day ]
                      which(res_names %in% not_available)
                    } )  %>% 
      unlist() %>% 
      as.numeric() %>%
      unique()
    
    id.final <- id.comb[! id.comb %in% id.rest]
    
    if(length(id.final) < n.list){
      day_res <- c(id.final, rep(NA, n.list))[seq_len(n.list)]
      warning("Pas assez d'interne pour assurer l'ensemble de gardes selon le protocole.")
    } else {
      nb_res_min <- sum(done[id.final] == min(done[id.final]))
      
      if( nb_res_min < n.list ) {
        day_res <- c( id.final[ done[id.final] == min(done[id.final]) ], 
                      sample( id.final[ done[id.final] > min(done[id.final]) ], 
                              n.list - nb_res_min) )
      } else {
        
        day_res <- sample(id.final[ done[id.final] == min(done[id.final]) ], 
                          size = n.list )
      }
    }

    oncal_list <- map2(oncal_list, res_names[day_res], function(x,y){
      x[d] <- y 
      x } )
    done[na.omit(day_res)] <- done[na.omit(day_res)] + planning$weight[d]
    
  }
  
  planning <- bind_cols( planning, bind_cols(oncal_list))
  return(list("planning" = planning, "done" = done))
}

###########
#     
#     # - ID of resident who are fresh (no recent oncal)
#     id.available <- id[available <= 0]
#     
#     # - ID resdident for week or week-end days
#     id.res <- id
# 
#     
#     # Get common ID between the 3 list
#     id.final <- id.res[ id.res %in% id.available & id.res %in% id.quota ]
#     
#     # Random selection of resident for one day.
#     # if there is not enough resident return NAs
#     if(length(id.final) >= n.list) {
#       
#       # Special selection for Thursday and big week-end
#       if ( planning$jour[day] == "Jeu" & planning$weight[day] < 1.5 ) {
#         if( sum(id_we %in% id.final) >= n.list){
#           id.final <- id_we[id_we %in% id.final]
#         } else if (sum(id_we %in% id.final) > 0){
#           id.final <- c(id_we[id_we %in% id.final], 
#                         sample(id.final, n.list - sum(id_we %in% id.final))
#           )
#         } 
#       }
#       
#       
#       
#       day_res <- sample(id.final, size = n.list ) 
#     } else {
#       day_res <- c(id.final, rep(NA, n.list))[n.list]
#       warning("Pas assez d'interne pour assurer l'ensemble de gardes selon le protocole.")
#     }
#     
#     # Complete planing, work done, off period.
#     oncal_list <- map2(oncal_list, res_names[day_res], c)
#     available[day_res] <- 15
#     done[day_res] <- done[day_res] + planning$weight[day]
#     
#     # Update rest of the brave !
#     available <- available - 1
#   }
#   
#   names(oncal_list) <- paste0("list", seq_len(n.list))
#   planning <- bind_cols( planning, bind_cols(oncal_list))
#   
#   return(list("planning" = planning, "done" = done))
#   
# }
